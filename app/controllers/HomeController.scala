package controllers

import business_layer.OrderManager
import models.{Order, User}

import javax.inject._
import play.api.mvc._

import java.sql.Timestamp
import java.util.Date

@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents,
                               orderManager: OrderManager) extends BaseController {

  def index() = Action.async { implicit request: Request[AnyContent] =>
      orderManager.createOrder(
          User(1, "Roma"),
          Order(1,"First", 1, new Timestamp(new Date().getTime))
      ).map(_ =>
    Ok(views.html.index()))(scala.concurrent.ExecutionContext.global)
  }
}
