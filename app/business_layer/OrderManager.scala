package business_layer

import db_layer.{DbImplicits, HasDatabaseConfig}
import db_layer.repos.{OrderRepo, UserRepo}
import models.{Order, User}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class OrderManager @Inject()(private[business_layer] val orderRepo: OrderRepo,
                             private[business_layer] val userRepo: UserRepo)(implicit ex: ExecutionContext)
    extends HasDatabaseConfig with DbImplicits {

    import profile.api._

    private[business_layer] val users = userRepo.table
    private[business_layer] val orders = orderRepo.table

    def createOrder(user: User, order: Order): Future[Order] =  {
        (for {
            _ <- userRepo.insert(user)
            newOrder <- orderRepo.insert(order)
        } yield newOrder).transactionally
    }.run

    def listOrders(id: User#IdType): Future[Seq[Order]] = ordersByUser(id).map(_._2).result.run

    def countOfOrders(id: User#IdType): Future[Int] = ordersByUser(id).length.result.run

    private[business_layer] def ordersByUser(id: User#IdType): Query[(userRepo.TableType, orderRepo.TableType), (User, Order), Seq] =
        users
            .join(orders).on(_.id === _.userId)
            .filter { case(user, _) => user.id === id}

    private def listOrders[business_layer]: Query[orderRepo.OrderTable, Order, Seq] = orderRepo.list()
}
