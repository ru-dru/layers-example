package models

import java.sql.Timestamp

case class Order(id: Int, title: String, userId: User#IdType, date: Timestamp) extends ModelWithId[Int]
