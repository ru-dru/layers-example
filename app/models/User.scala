package models

case class User(id: Int, name: String) extends ModelWithId[Int]
