package models

trait BasicModel
trait IdTrait[ID] {type IdType = ID}
trait ModelWithId[ID] extends BasicModel with IdTrait[ID] { val id: ID }

