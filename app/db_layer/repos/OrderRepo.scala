package db_layer.repos

import db_layer.tables.OrderModel
import models.Order
import slick.ast.BaseTypedType
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext

@Singleton
class OrderRepo @Inject() (override implicit val ex: ExecutionContext)
    extends BasicOneKeyedTableRepo[Order, Order#IdType]
        with OrderModel
        with WithInsertAction[Order] {

    override type TableType = OrderTable
    override val table = slick.lifted.TableQuery[TableType]
}
