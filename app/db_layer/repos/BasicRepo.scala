package db_layer.repos

import db_layer.HasDatabaseConfig
import models.{BasicModel, IdTrait, ModelWithId}
import slick.ast.BaseTypedType
import slick.jdbc.PostgresProfile
import slick.lifted.Rep

import scala.concurrent.ExecutionContext

trait Keyed[ID] extends IdTrait[ID] { def id: Rep[ID] }

trait DataRepo extends HasDatabaseConfig {
    implicit val ex: ExecutionContext
}

trait BasicOneTableRepo[M <: BasicModel] extends DataRepo {
    import profile.api._

    type TableType <: PostgresProfile.Table[M]
    val table: slick.lifted.TableQuery[TableType]

    def list(p: TableType => slick.lifted.Rep[Boolean] = _ => true): Query[TableType, M, Seq] = table.filter(p)
    def findFirst(p: TableType => slick.lifted.Rep[Boolean] = _ => true): DBIOAction[Option[M], NoStream, slick.dbio.Effect.Read] = list(p).result.headOption
}

trait BasicOneKeyedTableRepo[M <: ModelWithId[K], K] extends BasicOneTableRepo[M] {
    import profile.api._

    override type TableType <: PostgresProfile.Table[M] with Keyed[K]

    def getBy(id: K)(implicit _pkType: BaseTypedType[K]):  DBIOAction[Option[M], NoStream, slick.dbio.Effect.Read] = findFirst(_.id === id)
}

trait WithInsertAction[M <: BasicModel] extends BasicOneTableRepo[M] {
    import profile.api._

    def insert(e: M): DBIOAction[M, NoStream, slick.dbio.Effect.Write] = (table returning table) += e
}

trait WithDeleteAction[M <: BasicModel] extends BasicOneTableRepo[M] {
    import profile.api._

    def delete(p: TableType => slick.lifted.Rep[Boolean]): DBIOAction[Int, NoStream, slick.dbio.Effect.Write] = table.filter(p).delete
}

trait WithUpdateAction[M <: BasicModel] extends BasicOneTableRepo[M] {
    import profile.api._

    def update(e: M): DBIOAction[Int, NoStream, slick.dbio.Effect.Write] = table.update(e)
}
