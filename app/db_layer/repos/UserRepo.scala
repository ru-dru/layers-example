package db_layer.repos

import db_layer.tables.UserModel
import models.User
import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext

@Singleton
class UserRepo @Inject() (override implicit val ex: ExecutionContext)
    extends BasicOneKeyedTableRepo[User, User#IdType]
        with UserModel
        with WithInsertAction[User] {

    override type TableType = UserTable
    override val table = slick.lifted.TableQuery[TableType]
}
