package db_layer.tables

import db_layer.HasDatabaseConfig
import db_layer.repos.Keyed
import models.User
import slick.jdbc.PostgresProfile

trait UserModel { self: HasDatabaseConfig =>
    class UserTable(tag: slick.lifted.Tag)
        extends PostgresProfile.Table[User](tag, "users")
            with Keyed[User#IdType] {

        import profile.api._

        override def id = column[User#IdType]("id")

        def name = column[String]("name")

        override def * = (id, name).mapTo[User]
    }
}
