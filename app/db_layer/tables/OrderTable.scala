package db_layer.tables

import db_layer._
import db_layer.repos.Keyed
import models.{Order, User}
import slick.jdbc.PostgresProfile

import java.sql.Timestamp

trait OrderModel { self: HasDatabaseConfig =>
    class OrderTable(tag: slick.lifted.Tag)
        extends PostgresProfile.Table[Order](tag, "orders")
            with Keyed[Order#IdType] {

        import profile.api._

        override def id = column[Order#IdType]("id")

        def title = column[String]("title")

        def userId = column[User#IdType]("user_id")

        def date = column[Timestamp]("date")

        override def * = (id, title, userId, date).mapTo[Order]
    }
}
