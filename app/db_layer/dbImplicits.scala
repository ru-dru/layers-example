package db_layer

import slick.dbio.{DBIOAction, Effect, NoStream}

import scala.concurrent.Future

trait DbImplicits { self: HasDatabaseConfig =>
    implicit class DBIOImplicits[+R, S <: NoStream, -E <: Effect](dbio: DBIOAction[R, S, E]) {
        def run: Future[R] = db.run(dbio)
    }
}
