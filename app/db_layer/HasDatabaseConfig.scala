package db_layer

import slick.basic.DatabaseConfig
import slick.jdbc.PostgresProfile

trait HasDatabaseConfig {
    /** The Slick database configuration. */
    protected def dbConfig = DatabaseConfig.forConfig[PostgresProfile]("slick") // field is declared as a val because we want a stable identifier.

    /** The Slick profile extracted from `dbConfig`. */
    protected final lazy val profile: PostgresProfile = dbConfig.profile // field is lazy to avoid early initializer problems.

    /** The Slick database extracted from `dbConfig`. */
    protected final def db: PostgresProfile#Backend#Database = dbConfig.db
}
